import 'package:app_test/views/allow_location/view.dart';
import 'package:app_test/views/select_location_view/view.dart';

import '../helpers/extintions.dart';
import '../views/auth/sign_in/widgets/import.dart';
import '../views/auth/sign_up/view.dart';
import '../views/home_view/widgets/imports.dart';
import '../views/splash_view/view.dart';
import 'routes.dart';

import 'package:flutter/material.dart';

class AppRoutes {
  static AppRoutes get init => AppRoutes._internal();
  String initial = NamedRoutes.i.splash;

  AppRoutes._internal();
  Map<String, Widget Function(BuildContext context)> appRoutes = {
    NamedRoutes.i.splash: (context) => const SplashView(),
    NamedRoutes.i.home_view: (context) => HomeView(model: context.arg["model"]),
    NamedRoutes.i.sign_in: (context) => const SignInView(),
    NamedRoutes.i.sign_up: (context) => const SignUpView(),
    NamedRoutes.i.allow_location: (context) => const AllowLocationView(),
    NamedRoutes.i.select_location: (context) => const SelectLocationView(),

    // NamedRoutes.i.mu_droop: (context) => CustomMultiselectDropDown(),
    // NamedRoutes.i.home: (context) => const HomeView(),
    // NamedRoutes.i.tasks: (context) => TasksView(title: context.arg["title"]),
    // NamedRoutes.i.allProjects: (context) => const AllProjectsView(),
    // NamedRoutes.i.navbar: (context) => NavbarView(index: context.arg["index"]),
    // NamedRoutes.i.allFavKitchen: (context) => AllFavKitchenView(
    //     bloc: context.arg["bloc"], roomsBloc: context.arg["roomBloc"]),
    // NamedRoutes.i.projectDetails: (context) =>
    //     ProjectDetailsView(data: context.arg["data"]),
    // NamedRoutes.i.myRequests: (context) => const MyRequestsView(),
    // NamedRoutes.i.myReports: (context) => const MyReportsView(),
    // NamedRoutes.i.myAccount: (context) => const MyAccountView(),
    // NamedRoutes.i.myVaccations: (context) => const MyVaccationsView(),
    // NamedRoutes.i.myFines: (context) => const MyFinesView(),
    // NamedRoutes.i.myOvertime: (context) => const MyOvertimeView(),
    // NamedRoutes.i.allProducts: (context) => AllProducts(
    //     bloc: context.arg["bloc"],
    //     roomsBloc: context.arg["roomBloc"],
    //     kitchensBloc: context.arg["kitchensBloc"]),
    // NamedRoutes.i.success: (context) => SuccessView(
    //     title: context.arg["title"], subtitle: context.arg["subtitle"]),
  };
}
