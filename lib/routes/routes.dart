// ignore_for_file: non_constant_identifier_names

class NamedRoutes {
  static NamedRoutes get i => NamedRoutes._internal();

  NamedRoutes._internal();
  final splash = "/splash";
  final home_view = "/home_view";
  final sign_in = "/sign_in";
  final mu_droop = "/mu_droop";
  final sign_up = "/sign_up";
  final chat_view = "/chat_view";
  final allow_location = "/allow_location";
  final select_location = "/select_location";
}
