// this function is data controller
import '../models/home_model.dart';
import '../services/server_gate.dart';

class HomeRepository {
  static final HomeRepository instance = HomeRepository._internal();

  HomeRepository._internal();

  Future<HomeModel> data() async {
    ServerGate.i.addInterceptors();
    CustomResponse response = await ServerGate.i.getFromServer(
      url: 'sections/index',
    );
    try {
      if (response.success) {
        HomeModel model = HomeModel.fromJson(response.response!.data);
        return model;
      } else {
        throw Exception(response.response!.statusMessage);
      }
    } catch (e) {
      throw Exception(response.response!.statusMessage);
    }
  }

  Future<CustomResponse> fetchData() async {
    CustomResponse response = await ServerGate.i.getFromServer(
      url: 'sections/index',
    );

    return response;
  }
}
