import 'dart:async';
import 'dart:io';

import 'routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart' as lang;

import 'helpers/route.dart';
import 'helpers/theme.dart';
import 'injection_container.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  HttpOverrides.global = MyHttpOverrides();
  initKiwi();
  // Prefs = await SharedPreferences.getInstance();
  await lang.EasyLocalization.ensureInitialized();
  // await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  // GlobalNotification().setUpFirebase();
  Future.wait([
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]),
  ]).then((value) {
    runApp(
      lang.EasyLocalization(
        path: 'assets/langs',
        saveLocale: true,
        startLocale: const Locale('ar', 'SA'),
        fallbackLocale: const Locale('en', 'US'),
        supportedLocales: const [Locale('ar', 'SA'), Locale('en', 'US')],
        child: const MyApp(),
      ),
    );
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(393, 852),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, state) {
        return MaterialApp(
          title: 'ERP',
          // home: const MyHomePage(title: 'Flutter Demo Home Page'),
          theme: StylesApp.instance.getDarkTheme(context.locale),

          initialRoute: AppRoutes.init.initial,
          routes: AppRoutes.init.appRoutes,
          navigatorKey: navigator,
          debugShowCheckedModeBanner: false,
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          builder: (context, child) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(
                textScaleFactor:
                    context.locale.languageCode == "ar" ? 0.9.sp : 0.9.sp,
              ),
              child: _Unfocus(child: child!),
            );
          },
        );
      },
    );
  }
}

// ignore: non_constant_identifier_names
// late SharedPreferences Prefs;

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class _Unfocus extends StatefulWidget {
  const _Unfocus({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  __UnfocusState createState() => __UnfocusState();
}

class __UnfocusState extends State<_Unfocus> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: widget.child,
    );
  }
}
