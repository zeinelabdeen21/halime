
class Assets {
  Assets._();

  static final fonts = _AssetsFonts._();
  static final icons = _AssetsIcons._();
  static final langs = _AssetsLangs._();

}

class _AssetsFonts {
  _AssetsFonts._();


  final arbFontsSomarMediumOTF = 'assets/fonts/ArbFONTS-Somar-Medium.otf';
  final arbFontsSomarRegularOTF = 'assets/fonts/ArbFONTS-Somar-Regular.otf';
  final neoSansArabicRegularTTF = 'assets/fonts/NeoSansArabicRegular.ttf';
  final neoSansMediumTTF = 'assets/fonts/Neo_Sans_Medium.ttf';
  final alfontComSomarGxOTF = 'assets/fonts/alfont_com_SomarGX.otf';
}

class _AssetsIcons {
  _AssetsIcons._();


  final contactusPNG = 'assets/icons/Contactus.png';
  final splashPNG = 'assets/icons/Splash.png';
  final backgroundHomePNG = 'assets/icons/background_home.png';
  final chatprofilePNG = 'assets/icons/chatprofile.png';
  final checkCirclePNG = 'assets/icons/check-circle.png';
  final checkPNG = 'assets/icons/check.png';
  final fiSrSettingsSlidersPNG = 'assets/icons/fi-sr-settings-sliders.png';
  final loadingJSON = 'assets/icons/loading.json';
  final locationPanaPNG = 'assets/icons/location-pana.png';
  final locationIconSVG = 'assets/icons/location_icon.svg';
  final locationOnPNG = 'assets/icons/location_on.png';
  final logoPNG = 'assets/icons/logo.png';
  final logoHomePNG = 'assets/icons/logo_home.png';
}

class _AssetsLangs {
  _AssetsLangs._();


  final arSaJSON = 'assets/langs/ar-SA.json';
  final enUsJSON = 'assets/langs/en-US.json';
}
