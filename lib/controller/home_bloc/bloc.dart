import 'package:bloc/bloc.dart';

import '../../models/home_model.dart';
import '../../repository/repository.dart';
import '../../services/server_gate.dart';
import '../generic_bloc/generic_cubit.dart';

part 'event.dart';
part 'state.dart';

//  😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌 //

//----------------------------------------------------------------
//   هنا ان شاء الله ثلاث طرق لجلب الداتا اما بستخدام البلوك نيتف او اما بستخدام الجنرك بلوك او طريقة الكلين ا اركتكشر
//----------------------------------------------------------------

//  😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌😌 //

// this function is bloc naitev

class HomeBloc extends Bloc<HomeEvents, HomeStates> {
  HomeBloc() : super(HomeStates()) {
    // on<HomeEvents>((event, emit) async {
    //   await getData(event, emit);
    // });
    on<HomeEvents>(getData);

    add(HomeEventStart());
  }

  Future getData(HomeEvents event, Emitter<HomeStates> emit) async {
    if (event is HomeEventStart) {
      emit(HomeStateStart());

      CustomResponse response = await ServerGate.i.getFromServer(
        url: 'sections/index',
      );
      if (response.success) {
        HomeModel model = HomeModel.fromJson(response.response!.data);
        emit(HomeStateSuccess(model: model));
      } else {
        emit(HomeStateFailed(errType: response.errType!, msg: response.msg));
      }
    }
  }
}

class HomeBlocWithGenericBloc {
  final GenericBloc<HomeModel?> dataBloc = GenericBloc(null);

  data() async {
    CustomResponse response = await ServerGate.i.getFromServer(
      url: 'sections/index',
    );
    if (response.success) {
      HomeModel model = HomeModel.fromJson(response.response!.data);
      dataBloc.onUpdateData(model);
    }
  }
}

// mvvm ================================THIS TEST MVVM================================

// this function is bloc with cubit and GenericBloc
class HomeController {
  final GenericBloc<HomeModel?> offersDetailsBloc = GenericBloc(null);
  void getData() async {
    try {
      var homeBloc = await RepositoryInterface.instance.getData();

      offersDetailsBloc.onUpdateData(homeBloc);
    } catch (e) {
      offersDetailsBloc.onFailedResponse(error: e.toString());
    }
  }
}

// this function is bloc naitev mvvm
class HomeBlocMvvm extends Bloc<HomeEvents, HomeStates> {
  HomeBlocMvvm() : super(HomeStates()) {
    on<HomeEvents>(getData);

    add(HomeEventStart());
  }

  Future getData(HomeEvents event, Emitter<HomeStates> emit) async {
    if (event is HomeEventStart) {
      emit(HomeStateStart());

      CustomResponse response = await RepositoryInterface.instance.fetchData();

      if (response.success) {
        HomeModel model = HomeModel.fromJson(response.response!.data);

        emit(HomeStateSuccess(model: model));
      } else {
        emit(HomeStateFailed(
          errType: response.statusCode,
          msg: response.msg,
        ));
      }
    }
  }
}
