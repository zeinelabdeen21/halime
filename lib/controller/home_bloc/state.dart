part of 'bloc.dart';

class HomeStates {}

class HomeStateStart extends HomeStates {}

class HomeStateSuccess extends HomeStates {
  HomeModel model;
  HomeStateSuccess({
    required this.model,
  });
}

class HomeStateFailed extends HomeStates {
  String msg;
  int errType;
  HomeStateFailed({
    required this.errType,
    required this.msg,
  });
}
