// ignore_for_file: must_be_immutable, override_on_non_overriding_member

part of 'bloc.dart';

abstract class SignInEvent {}

class LoginEventStart extends SignInEvent {
  String phone;
  String password;
  GlobalKey<FormState> formKey;
  LoginEventStart({
    required this.phone,
    required this.password,
    required this.formKey,
  });
}
