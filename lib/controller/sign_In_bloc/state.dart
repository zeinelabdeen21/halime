// ignore_for_file: must_be_immutable

part of 'bloc.dart';

class SignInState {}

class LoginStateStart extends SignInState {}

class LoginStateSuccess extends SignInState {
  final LoginModel model;
  LoginStateSuccess({
    required this.model,
  });
}

class LoginStateFailed extends SignInState {
  String msg;
  int errType;
  LoginStateFailed({
    required this.msg,
    required this.errType,
  });
}
