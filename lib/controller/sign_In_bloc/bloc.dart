// ignore_for_file: unused_element

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../helpers/CustomButtonAnimation/CustomButtonAnimation.dart';
import '../../helpers/flash_helper.dart';
import '../../helpers/route.dart';
import '../../models/model.dart';
import '../../routes/routes.dart';
import '../../services/server_gate.dart';
import '../cubit/generic_cubit.dart';
import '../generic_bloc/generic_cubit.dart';

part 'event.dart';
part 'state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  SignInBloc() : super(SignInState()) {
    on<LoginEventStart>(_sendData);
  }

  final TextEditingController phone = TextEditingController();
  final TextEditingController password = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey<CustomButtonState>();

  GenericBloc<bool> newBool = GenericBloc(false);
  GenericBloc<bool> isShowPassword = GenericBloc(true);
  GenericCubit<bool> isShowPassword22 = GenericCubit(true);
  GenericBloc<LoginModel> loginModel = GenericBloc(LoginModel());

  void _sendData(
    LoginEventStart event,
    Emitter<SignInState> emit,
  ) async {
    // add loading start
    btnKey.currentState!.animateForward();
    emit(LoginStateStart());

    CustomResponse response = await ServerGate.i.sendToServer(
      url: 'user/login',
      body: {'phone': event.phone, 'password': event.password},
    );

    btnKey.currentState!.animateForward();
    if (response.success) {
      LoginModel model = LoginModel.fromJson(response.response!.data);

      FlashHelper.successBar(message: model.message, title: model.message);
      await pushAndRemoveUntil(
        NamedRoutes.i.home_view,
      );

      btnKey.currentState!.animateReverse();

      emit(LoginStateSuccess(model: model));
    } else {
      btnKey.currentState!.animateReverse();
      FlashHelper.errorBar(message: response.msg, title: response.msg);
      emit(LoginStateFailed(
        errType: response.errType!,
        msg: response.msg,
      ));
    }
  }

  void oK() {
    if (formKey.currentState!.validate()) {
      add(LoginEventStart(
        phone: phone.text,
        password: password.text,
        formKey: formKey,
      ));
    }
  }
}
