import 'package:flutter_bloc/flutter_bloc.dart';

import 'generic_cubit_state.dart';

class GenericCubit<T> extends Cubit<GenericCubitState<T>> {
  GenericCubit(T data) : super(GenericInitialState<T>(data));

  onUpdateData(T data) {
    emit(GenericUpdateState<T>(data, state.error, state.status));
  }

  onFailedResponse({String error = ""}) {
    emit(GenericFailedState<T>(state.data, error, state.status));
  }

  onUpdateToInitState(T data) {
    emit(GenericInitialState<T>(data));
  }
}
