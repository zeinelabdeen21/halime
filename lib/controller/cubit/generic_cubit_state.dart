import 'package:flutter/foundation.dart' show immutable;

enum Status { empty, loading, failed, success }

@immutable
class GenericCubitState<T> {
  final T data;
  final String error;

  final Status status;

  const GenericCubitState(this.data, this.error, this.status);

  // factory GenericCubitState.empty() =>
  //     const GenericCubitState(status: Status.empty);

  // factory GenericCubitState.loading() =>
  //     const GenericCubitState(status: Status.loading);

  // factory GenericCubitState.failure(String error) =>
  //     GenericCubitState(error: error, status: Status.failure);

  // factory GenericCubitState.success(T? data) =>
  //     GenericCubitState(data: data, status: Status.success);

  @override
  List<Object> get props => [status];
}

class GenericInitialState<T> extends GenericCubitState<T> {
  const GenericInitialState(T data) : super(data, "", Status.empty);

  @override
  List<Object> get props => [status];
}

class GenericLodingState<T> extends GenericCubitState<T> {
  const GenericLodingState(T data) : super(data, "", Status.loading);

  @override
  List<Object> get props => [status];
}

class GenericUpdateState<T> extends GenericCubitState<T> {
  const GenericUpdateState(T data, String error, Status status)
      : super(data, error, Status.success);

  @override
  List<Object> get props => [status];
}

class GenericFailedState<T> extends GenericCubitState<T> {
  const GenericFailedState(T data, String error, Status status)
      : super(data, error, Status.failed);

  @override
  List<Object> get props => [error];
}
