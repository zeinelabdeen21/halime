part of 'bloc.dart';

abstract class SplashEvent extends Equatable {}

class SplashInitialEvent extends SplashEvent {
  @override
  List<Object?> get props => [];
}
