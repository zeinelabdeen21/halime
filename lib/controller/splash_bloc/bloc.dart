// ignore_for_file: unused_local_variable

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../helpers/route.dart';

import '../../routes/routes.dart';

part 'event.dart';
part 'state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() : super(SplashState()) {
    on<SplashInitialEvent>(_onInitialize);
  }

  _onInitialize(
    SplashInitialEvent event,
    Emitter<SplashState> emit,
  ) async {
    Future.delayed(const Duration(seconds: 2), () async {
      await replacement(
        NamedRoutes.i.allow_location,
        // arguments: {"model": model},
        // type: NavigatorAnimation.scale,
      );
    });
  }
}
