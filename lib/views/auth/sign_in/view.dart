// ignore_for_file: unused_local_variable

part of './widgets/import.dart';

class SignInView extends StatefulWidget {
  const SignInView({super.key});

  @override
  State<SignInView> createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  final bloc = KiwiContainer().resolve<SignInBloc>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.amberAccent,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.amberAccent,
        title: const Text("login"),
      ),
      body: Column(
        children: [
          SizedBox(height: 15.h),
          CustomFormSignIn(bloc: bloc),
          SizedBox(height: 15.h),
          CustomBtn(bloc: bloc),
          SizedBox(height: 15.h),
        ],
      ),
    );
  }
}
