// ignore_for_file: avoid_print

part of "../widgets/import.dart";

class CustomFormSignIn extends StatefulWidget {
  final SignInBloc bloc;
  const CustomFormSignIn({super.key, required this.bloc});

  @override
  State<CustomFormSignIn> createState() => _CustomFormSignInState();
}

class _CustomFormSignInState extends State<CustomFormSignIn> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.bloc.formKey,
      child: Column(
        children: [
          SizedBox(height: 15.h),
          BlocSelector(
            bloc: widget.bloc,
            selector: (state) {
              print("zein ${widget.bloc.phone.text}");
              return true;
            },
            builder: (context, state) {
              return txtField(
                controller: widget.bloc.phone,
                validator: (v) {
                  if (v!.isEmpty) {
                    return tr(LocaleKeys.Mobile_number);
                  } else {
                    return null;
                  }
                },
                onSaved: (o) {},
                hintText: tr(LocaleKeys.Mobile_number),
                enabled: true,
                obscureText: false,
                textInputType: TextInputType.phone,
              );
            },
          ),
          SizedBox(height: 15.h),
          BlocBuilder<GenericCubit<bool>, GenericCubitState<bool>>(
            buildWhen: (previous, current) {
              // only rebuild when the value is changed

              print("zeineeee ${current.data}");
              return previous.data != current.data;
            },
            bloc: widget.bloc.isShowPassword22,
            builder: (context, state) {
              return txtFieldPass(
                suffix: InkWell(
                  child: state.data
                      ? const Icon(
                          Icons.visibility,
                          color: Colors.grey,
                        )
                      : const Icon(
                          Icons.visibility_off,
                          color: Colors.grey,
                        ),
                  onTap: () {
                    print(state.status);
                    editUser();
                    // widget.bloc.isShowPassword22.onUpdateData(!state.data);
                  },
                ),
                controller: widget.bloc.password,
                validator: (v) {
                  if (v!.isEmpty) {
                    return tr(LocaleKeys.Forgot_your_password);
                  } else {
                    return null;
                  }
                },
                onSaved: (o) {},
                hintText: tr(LocaleKeys.password),
                enabled: true,
                obscureText: widget.bloc.isShowPassword22.state.data,
                textInputType: TextInputType.visiblePassword,
              );
            },
          ),
        ],
      ),
    );
  }

  void editUser() async {
    bool isAccepted = await deleteDialog(context);
    if (isAccepted) {
      if (!mounted) return;

      showDialog(
        context: context,
        builder: (_) {
          return BlocBuilder<GenericCubit<bool>, GenericCubitState<bool>>(
            bloc: widget.bloc.isShowPassword22,
            builder: (context, state) {
              return switch (state.status) {
                Status.empty => const SizedBox(),
                Status.loading => const ProgressDialog(
                    title: "Deleting user...",
                    isProgressed: true,
                  ),
                Status.failed => RetryDialog(
                    title: state.error ?? "Error",
                    onRetryPressed: () {},
                  ),
                Status.success => ProgressDialog(
                    title: "Successfully deleted",
                    onPressed: () {
                      widget.bloc.isShowPassword22.onUpdateData(!state.data);

                      Navigator.pop(context);
                    },
                    isProgressed: false,
                  ),
              };
            },
          );
        },
      );
    }
  }
}
