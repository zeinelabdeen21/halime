import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kiwi/kiwi.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../assets.dart';
import '../../../../controller/cubit/generic_cubit.dart';
import '../../../../controller/cubit/generic_cubit_state.dart';
import '../../../../controller/sign_In_bloc/bloc.dart';

import '../../../../helpers/CustomButtonAnimation/LoadingButton.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../../generated/locale_keys.g.dart';
import '../../../../helpers/custom_btn.dart';
import '../../../../helpers/dialog/delete_dialog.dart';
import '../../../../helpers/dialog/progress_dialog.dart';
import '../../../../helpers/dialog/retry_dialog.dart';
import '../../../../helpers/route.dart';
import '../../../../helpers/text_form_field.dart';
import '../../../../routes/routes.dart';

part '../view.dart';
part './custom_button.dart';
part 'custom_form_sign_n.dart';
