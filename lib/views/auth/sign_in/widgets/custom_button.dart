part of "../widgets/import.dart";

class CustomBtn extends StatelessWidget {
  final SignInBloc bloc;
  const CustomBtn({super.key, required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder(
          bloc: bloc,
          builder: (context, state) => LoadingButton(
            btnKey: bloc.btnKey,
            title: tr(LocaleKeys.Login),
            onTap: () {
              bloc.oK();
            },
            color: const Color.fromARGB(255, 183, 118, 4),
            textColor: const Color.fromARGB(255, 246, 246, 246),
            margin: const EdgeInsets.symmetric(vertical: 10),
            fontFamily: Assets.fonts.arbFontsSomarMediumOTF,
            fontSize: 15,
          ),
        ),
        CustomElevatedButton(
          text: tr(LocaleKeys.Create_an_account),
          onPressed: () {
            push(
              NamedRoutes.i.sign_up,
              type: NavigatorAnimation.scale,
            );
          },
        ),
      ],
    );
  }
}
