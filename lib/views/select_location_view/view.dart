import 'dart:async';

import 'package:app_test/assets.dart';
import 'package:app_test/helpers/extintions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:map_picker/map_picker.dart';

import '../../helpers/custom_btn.dart';
import '../../helpers/route.dart';
import '../../helpers/theme.dart';
import '../../routes/routes.dart';

class SelectLocationView extends StatefulWidget {
  const SelectLocationView({super.key});

  @override
  State<SelectLocationView> createState() => _SelectLocationViewState();
}

class _SelectLocationViewState extends State<SelectLocationView> {
  final _controller = Completer<GoogleMapController>();
  MapPickerController mapPickerController = MapPickerController();

  CameraPosition cameraPosition = const CameraPosition(
    target: LatLng(41.311158, 69.279737),
    zoom: 14.4746,
  );

  var textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          MapPicker(
            // pass icon widget
            iconWidget: Image.asset(
              Assets.icons.locationOnPNG,
              height: 60,
            ),
            //add map picker controller
            mapPickerController: mapPickerController,
            child: GoogleMap(
              myLocationEnabled: true,
              zoomControlsEnabled: false,
              // hide location button
              myLocationButtonEnabled: false,
              mapType: MapType.normal,
              //  camera position
              initialCameraPosition: cameraPosition,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              onCameraMoveStarted: () {
                // notify map is moving
                mapPickerController.mapMoving!();
                textController.text = "checking ...";
              },
              onCameraMove: (cameraPosition) {
                this.cameraPosition = cameraPosition;
              },
              onCameraIdle: () async {
                // notify map stopped moving
                mapPickerController.mapFinishedMoving!();
                //get address name from camera position
                List<Placemark> placemarks = await placemarkFromCoordinates(
                  cameraPosition.target.latitude,
                  cameraPosition.target.longitude,
                );

                // update the ui with the address
                textController.text =
                    '${placemarks.first.name}, ${placemarks.first.administrativeArea}, ${placemarks.first.country}';
              },
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).viewPadding.top + 20,
            width: MediaQuery.of(context).size.width - 50,
            height: 50,
            child: TextFormField(
              textAlign: TextAlign.center,
              readOnly: true,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(15),
                  ),
                  borderSide: BorderSide(
                    width: 0.5,
                    style: BorderStyle.solid,
                    color: "#0E1116".toColor,
                  ),
                ),
                errorStyle: const TextStyle(
                  // fontFamily: AppTheme.boldFont,
                  color: Color(0xFFC1C1C1),
                  fontSize: 13,
                ),
                prefixIcon: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Icon(
                    Icons.search,
                    size: 18,
                    color: "#74F5B2".toColor,
                  ),
                ),
                contentPadding: const EdgeInsets.only(
                    left: 15, top: 20, bottom: 20, right: 15),
                border: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(15),
                  ),
                  borderSide: BorderSide(
                    width: 0.5,
                    style: BorderStyle.solid,
                    color: "#0E1116".toColor,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(15),
                  ),
                  borderSide: BorderSide(
                    width: 0.5,
                    style: BorderStyle.solid,
                    color: "#0E1116".toColor,
                  ),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(15),
                  ),
                  borderSide: BorderSide(
                    width: 0.5,
                    style: BorderStyle.solid,
                    color: "#0E1116".toColor,
                  ),
                ),
                filled: true,
                fillColor: "#0E1116".toColor,
                enabled: true,

                // labelText: "المنطقه أو الشارع",
                labelStyle: const TextStyle(
                  color: Color(0xFFC1C1C1),
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                ),
              ),
              controller: textController,
            ),
          ),
          Positioned(
            bottom: 220,
            left: 60.w,
            right: 60.w,
            child: SizedBox(
              height: 60.h,
              child: TextButton(
                onPressed: () {
                  print(
                      "Location ${cameraPosition.target.latitude} ${cameraPosition.target.longitude}");
                  print("Address: ${textController.text}");
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>("#375CF2".toColor),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.my_location,
                      color: Colors.white,
                      size: 18,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      "استخدم الموقع الحالي",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: "#FFFFFF".toColor,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
      bottomSheet: Container(
        decoration: BoxDecoration(
          color: "#0E1116".toColor,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
        ),
        height: 180,
        padding:
            const EdgeInsets.only(top: 20, bottom: 20, left: 25, right: 25),
        // height: 150.h,

        child: Column(
          children: [
            Row(
              children: [
                const Icon(
                  Icons.location_on,
                  size: 25,
                  color: Colors.white,
                ),
                const SizedBox(
                  width: 9,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "موقعك الحالي",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: "#74F5B2".toColor,
                      ),
                    ),
                    Text(
                      "22 شارع جامعة الدول العربية",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: "#8D8D8D".toColor,
                      ),
                    ),
                  ],
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            CustomElevatedButton(
              height: 60.h,
              text: "تأكيد الموقع",
              onPressed: () {
                push(
                  NamedRoutes.i.home_view,
                  type: NavigatorAnimation.scale,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
