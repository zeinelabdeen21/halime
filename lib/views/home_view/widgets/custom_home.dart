part of "imports.dart";

class CustomHome extends StatelessWidget {
  final HomeBlocMvvm bloc;
  const CustomHome({super.key, required this.bloc});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: bloc,
      builder: (context, state) {
        if (state is HomeStateStart) {
          return const Center(
            child: LoadingBtn(),
          );
        } else if (state is HomeStateSuccess) {
          return ListView.builder(
            itemCount: state.model.data.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, i) {
              return Padding(
                padding: const EdgeInsets.only(
                    left: 15, right: 15, bottom: 2, top: 2),
                child: ExpansionTile(
                    backgroundColor: const Color.fromARGB(83, 248, 248, 248),
                    collapsedBackgroundColor:
                        const Color.fromARGB(22, 186, 21, 21),
                    collapsedIconColor: const Color.fromARGB(255, 134, 97, 97),
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    collapsedShape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(50))),
                    title: Container(
                      height: 50,
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(),
                      // color: Color(0xFFBA1615),
                      child: Row(
                        children: [
                          Image.network(
                            state.model.data[i].image,
                            width: 50.w,
                            fit: BoxFit.contain,
                          ),
                          SizedBox(
                            width: 20.w,
                          ),
                          Text(
                            state.model.data[i].titleAr,
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                    ),
                    children: [
                      if (state.model.data[i].subsection.isEmpty)
                        Padding(
                          padding: const EdgeInsets.only(
                              right: 30, left: 30, bottom: 10),
                          child: Container(
                            height: 50,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: const Color(0xFFFFF2F2),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: const Text("لا توجد أقسام"),
                          ),
                        ),
                      ...List.generate(
                        state.model.data[i].subsection.length,
                        (index) => Padding(
                          padding: const EdgeInsets.only(
                              right: 30, left: 30, bottom: 10),
                          child: InkWell(
                            onTap: () {},
                            child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                color: const Color(0xFFFFF2F2),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                state.model.data[i].subsection[index].titleAr,
                                style: const TextStyle(
                                    color: Color(0xFF394739), fontSize: 15),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ]),
              );
            },
          );
        } else if (state is HomeStateFailed) {
          return Center(
            child: Text(state.msg),
          );
        } else {
          return const Center(
            child: SizedBox(
              width: 30,
              height: 30,
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
    // return BlocBuilder<GenericBloc<HomeModel?>, GenericState<HomeModel?>>(
    //   bloc: bloc.offersDetailsBloc,
    //   builder: (BuildContext context, state) {
    //     if (state is GenericUpdateState) {
    //       if (state.data != null) {
    //         return ListView.builder(
    //           shrinkWrap: true,
    //           physics: const NeverScrollableScrollPhysics(),
    //           itemCount: state.data!.data.length,
    //           itemBuilder: (context, i) {
    //             return Padding(
    //               padding: const EdgeInsets.only(
    //                   left: 15, right: 15, bottom: 2, top: 2),
    //               child: ExpansionTile(
    //                   backgroundColor: const Color.fromARGB(83, 248, 248, 248),
    //                   collapsedBackgroundColor:
    //                       const Color.fromARGB(22, 186, 21, 21),
    //                   collapsedIconColor:
    //                       const Color.fromARGB(255, 134, 97, 97),
    //                   shape: const RoundedRectangleBorder(
    //                       borderRadius: BorderRadius.all(Radius.circular(20))),
    //                   collapsedShape: const RoundedRectangleBorder(
    //                       borderRadius: BorderRadius.all(Radius.circular(50))),
    //                   title: Container(
    //                     height: 50,
    //                     alignment: Alignment.center,
    //                     decoration: const BoxDecoration(),
    //                     // color: Color(0xFFBA1615),
    //                     child: Row(
    //                       children: [
    //                         Image.network(
    //                           state.data!.data[i].image,
    //                           width: 50.w,
    //                           fit: BoxFit.contain,
    //                         ),
    //                         SizedBox(
    //                           width: 20.w,
    //                         ),
    //                         Text(
    //                           state.data!.data[i].titleAr,
    //                           style: const TextStyle(
    //                             color: Colors.black,
    //                             fontWeight: FontWeight.bold,
    //                             fontSize: 15,
    //                           ),
    //                         ),
    //                       ],
    //                     ),
    //                   ),
    //                   children: [
    //                     if (state.data!.data[i].subsection.isEmpty)
    //                       Padding(
    //                         padding: const EdgeInsets.only(
    //                             right: 30, left: 30, bottom: 10),
    //                         child: Container(
    //                           height: 50,
    //                           alignment: Alignment.center,
    //                           decoration: BoxDecoration(
    //                             color: const Color(0xFFFFF2F2),
    //                             borderRadius: BorderRadius.circular(15),
    //                           ),
    //                           child: const Text("لا توجد أقسام"),
    //                         ),
    //                       ),
    //                     ...List.generate(
    //                       state.data!.data[i].subsection.length,
    //                       (index) => Padding(
    //                         padding: const EdgeInsets.only(
    //                             right: 30, left: 30, bottom: 10),
    //                         child: InkWell(
    //                           onTap: () {},
    //                           child: Container(
    //                             height: 50,
    //                             decoration: BoxDecoration(
    //                               color: const Color(0xFFFFF2F2),
    //                               borderRadius: BorderRadius.circular(15),
    //                             ),
    //                             alignment: Alignment.center,
    //                             child: Text(
    //                               state.data!.data[i].subsection[index].titleAr,
    //                               style: const TextStyle(
    //                                   color: Color(0xFF394739), fontSize: 15),
    //                             ),
    //                           ),
    //                         ),
    //                       ),
    //                     ),
    //                   ]),
    //             );
    //           },
    //         );
    //       } else {
    //         return Text("1");
    //       }
    //     } else {
    //       return Text("2222");
    //     }
    //   },
    // );
  }

  // BlocBuilder<HomeBloc, Object?> _dataModel() {
  //   return BlocBuilder(
  //   bloc: bloc,
  //   builder: (context, state) {
  //     if (state is HomeStateStart) {
  //       return const Center(
  //         child: LoadingBtn(),
  //       );
  //     } else if (state is HomeStateSuccess) {
  //       return ListView.builder(
  //         itemCount: state.data!.data.length,
  //         shrinkWrap: true,
  //         physics: const NeverScrollableScrollPhysics(),
  //         itemBuilder: (context, i) {
  // return Padding(
  //   padding: const EdgeInsets.only(
  //       left: 15, right: 15, bottom: 2, top: 2),
  //   child: ExpansionTile(
  //       backgroundColor: const Color.fromARGB(83, 248, 248, 248),
  //       collapsedBackgroundColor:
  //           const Color.fromARGB(22, 186, 21, 21),
  //       collapsedIconColor: const Color.fromARGB(255, 134, 97, 97),
  //       shape: const RoundedRectangleBorder(
  //           borderRadius: BorderRadius.all(Radius.circular(20))),
  //       collapsedShape: const RoundedRectangleBorder(
  //           borderRadius: BorderRadius.all(Radius.circular(50))),
  //       title: Container(
  //         height: 50,
  //         alignment: Alignment.center,
  //         decoration: const BoxDecoration(),
  //         // color: Color(0xFFBA1615),
  //         child: Row(
  //           children: [
  //             Image.network(
  //               state.data!.data[i].image,
  //               width: 50.w,
  //               fit: BoxFit.contain,
  //             ),
  //             SizedBox(
  //               width: 20.w,
  //             ),
  //             Text(
  //               state.data!.data[i].titleAr,
  //               style: const TextStyle(
  //                 color: Colors.black,
  //                 fontWeight: FontWeight.bold,
  //                 fontSize: 15,
  //               ),
  //             ),
  //           ],
  //         ),
  //       ),
  //       children: [
  //         if (state.data!.data[i].subsection.isEmpty)
  //           Padding(
  //             padding: const EdgeInsets.only(
  //                 right: 30, left: 30, bottom: 10),
  //             child: Container(
  //               height: 50,
  //               alignment: Alignment.center,
  //               decoration: BoxDecoration(
  //                 color: const Color(0xFFFFF2F2),
  //                 borderRadius: BorderRadius.circular(15),
  //               ),
  //               child: const Text("لا توجد أقسام"),
  //             ),
  //           ),
  //         ...List.generate(
  //           state.data!.data[i].subsection.length,
  //           (index) => Padding(
  //             padding: const EdgeInsets.only(
  //                 right: 30, left: 30, bottom: 10),
  //             child: InkWell(
  //               onTap: () {},
  //               child: Container(
  //                 height: 50,
  //                 decoration: BoxDecoration(
  //                   color: const Color(0xFFFFF2F2),
  //                   borderRadius: BorderRadius.circular(15),
  //                 ),
  //                 alignment: Alignment.center,
  //                 child: Text(
  //                   state.data!.data[i].subsection[index].titleAr,
  //                   style: const TextStyle(
  //                       color: Color(0xFF394739), fontSize: 15),
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ),
  //       ]),
  // );
  //         },
  //       );
  //     } else if (state is HomeStateFailed) {
  //       return Center(
  //         child: Text(state.msg),
  //       );
  //     } else {
  //       return Center(
  //         child: Container(
  //           width: 30,
  //           height: 30,
  //           child: const CircularProgressIndicator(),
  //         ),
  //       );
  //     }
  //   },
  // );
  // }
}
