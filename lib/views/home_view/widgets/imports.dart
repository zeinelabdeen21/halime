import 'package:app_test/helpers/extintions.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../assets.dart';

import '../../../controller/home_bloc/bloc.dart';

import '../../../helpers/CustomButtonAnimation/CustomButtonAnimation.dart';

import '../../../helpers/loading_app.dart';

import '../../../models/model.dart';

part '../view.dart';
part 'custom_home.dart';
