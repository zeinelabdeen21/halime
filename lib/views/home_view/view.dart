// ignore_for_file: prefer_final_fields

part of './widgets/imports.dart';

class HomeView extends StatefulWidget {
  final LoginModel? model;
  const HomeView({super.key, required this.model});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final GlobalKey<CustomButtonState> btnKey = GlobalKey<CustomButtonState>();
  // HomeBlocMvvm _bloc2 = KiwiContainer().resolve<HomeBlocMvvm>();
  // HomeController _bloc = HomeController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            decoration: const BoxDecoration(
              color: Color(0xFF0E1116),
            ),
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        Assets.icons.backgroundHomePNG,
                      ),
                    ),
                  ),
                  child: SizedBox(
                    width: 481,
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(25, 19, 25, 78),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            padding: const EdgeInsets.fromLTRB(15.7, 0, 0, 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: const Color(0xFF0E1116),
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Color(0x40FFFFFF),
                                        offset: Offset(0, 0),
                                        blurRadius: 5,
                                      ),
                                    ],
                                  ),
                                  child: Container(
                                    width: 46,
                                    height: 46,
                                    child: Icon(
                                      Icons.settings,
                                      color: "#74F5B2".toColor,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.fromLTRB(
                                      0, 14.5, 17.2, 9.5),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.location_on,
                                        color: "#FFFFFF".toColor,
                                        size: 25,
                                      ),
                                      const SizedBox(
                                        width: 12,
                                      ),
                                      const Text(
                                        '22 شارع جامعة الدول العربية',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18,
                                          color: Color(0xFFFFFFFF),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.fromLTRB(0, 20, 0, 29),
                            child: Image.asset(
                              Assets.icons.logoHomePNG,
                              width: 220.w,
                              fit: BoxFit.contain,
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              decoration: BoxDecoration(
                                color: const Color(0xFF0E1116),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: SizedBox(
                                width: double.infinity,
                                child: Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(19, 18, 16, 18),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 20,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 0, 0, 0.6),
                                              child: SizedBox(
                                                width: 20,
                                                height: 6.3,
                                                child: Icon(
                                                  Icons.search,
                                                  size: 20,
                                                  color: "#74F5B2".toColor,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 0, 0, 0.6),
                                              child: const SizedBox(
                                                width: 20,
                                                height: 6.3,
                                                // child: SvgPicture.network(
                                                //   'assets/vectors/vector_83_x2.svg',
                                                // ),
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 0, 0, 0),
                                              child: const SizedBox(
                                                width: 20,
                                                height: 6.3,
                                                // child: SvgPicture.network(
                                                //   'assets/vectors/vector_52_x2.svg',
                                                // ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: const EdgeInsets.fromLTRB(
                                              0, 1, 0, 1.9),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                margin:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 2, 15, 1.1),
                                                child: const Text(
                                                  'البحث عن ملاعب',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12,
                                                    color: Color(0xFF8D8D8D),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 17,
                                        height: 17.1,
                                        child: SizedBox(
                                          width: 17,
                                          height: 17.1,
                                          child: Image.asset(
                                            Assets.icons.fiSrSettingsSlidersPNG,
                                            width: 220.w,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Color(0xFF0E1116),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    child: SizedBox(
                      width: 393,
                      height: 540.h,
                      child: Container(
                        padding: const EdgeInsets.fromLTRB(21, 23, 25, 4),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 22),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: SizedBox(
                                  width: 327.5,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Text(
                                        'الاقرب لك',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 20,
                                          color: Color(0xFFFFFFFF),
                                        ),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 5, 7, 5),
                                        child: const Text(
                                          'عرض المزيد',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                            decoration:
                                                TextDecoration.underline,
                                            color: Color(0xFF74F5B2),
                                            decorationColor: Color(0xFF74F5B2),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 21),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: const Color(0xFFFFFFFF),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 8, 0, 18),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            13, 0, 18.8, 14),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin:
                                                      const EdgeInsets.fromLTRB(
                                                          0, 7.1, 4, 7.3),
                                                  child: SizedBox(
                                                    width: 57,
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          margin:
                                                              const EdgeInsets
                                                                  .fromLTRB(
                                                                  0, 0, 3, 0),
                                                          width: 12,
                                                          height: 11.6,
                                                          child: const SizedBox(
                                                            width: 12,
                                                            height: 11.6,
                                                            // child: SvgPicture
                                                            //     .network(
                                                            //   'assets/vectors/vector_30_x2.svg',
                                                            // ),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              const EdgeInsets
                                                                  .fromLTRB(
                                                                  0, 0, 3, 0),
                                                          width: 12,
                                                          height: 11.6,
                                                          child: const SizedBox(
                                                            width: 12,
                                                            height: 11.6,
                                                            // child: SvgPicture
                                                            //     .network(
                                                            //   'assets/vectors/vector_49_x2.svg',
                                                            // ),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              const EdgeInsets
                                                                  .fromLTRB(
                                                                  0, 0, 3, 0),
                                                          width: 12,
                                                          height: 11.6,
                                                          child: const SizedBox(
                                                            width: 12,
                                                            height: 11.6,
                                                            // child: SvgPicture
                                                            //     .network(
                                                            //   'assets/vectors/vector_56_x2.svg',
                                                            // ),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: 12,
                                                          height: 11.6,
                                                          child: const SizedBox(
                                                            width: 12,
                                                            height: 11.6,
                                                            // child: SvgPicture
                                                            //     .network(
                                                            //   'assets/vectors/vector_7_x2.svg',
                                                            // ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                const Text(
                                                  '4.0',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14,
                                                    color: Color(0xFF8D8D8D),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 9, 0, 0),
                                              child: const Text(
                                                'ملعب تنس',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 14,
                                                  color: Color(0xFF0E1116),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 7, 0, 3),
                                              child: const SizedBox(
                                                width: 20.2,
                                                height: 16,
                                                // child: SvgPicture.network(
                                                //   'assets/vectors/group_3_x2.svg',
                                                // ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 12),
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            color: Color(0xFF0E1116),
                                          ),
                                          child: Container(
                                            width: 347,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.fromLTRB(
                                            16, 0, 16, 0),
                                        child: SizedBox(
                                          width: 267,
                                          child: Stack(
                                            children: [
                                              SizedBox(
                                                width: 267,
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      margin: const EdgeInsets
                                                          .fromLTRB(
                                                          0, 0, 9.5, 61),
                                                      child: const SizedBox(
                                                        width: 138.3,
                                                        child: Text(
                                                          'ملاعب تنس النادي الأهلي',
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontSize: 16,
                                                            color: Color(
                                                                0xFF0E1116),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: const EdgeInsets
                                                          .fromLTRB(0, 1, 0, 0),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Container(
                                                            margin:
                                                                const EdgeInsets
                                                                    .fromLTRB(
                                                                    0,
                                                                    25,
                                                                    11.3,
                                                                    10.7),
                                                            child: Column(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Container(
                                                                  margin: const EdgeInsets
                                                                      .fromLTRB(
                                                                      0,
                                                                      0,
                                                                      0.7,
                                                                      2.3),
                                                                  child:
                                                                      const Text(
                                                                    'مجمع ملاعب متمييز علي اعلي مستوي ',
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right,
                                                                    style:
                                                                        TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      fontSize:
                                                                          12,
                                                                      color: Color(
                                                                          0xFF8D8D8D),
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: const EdgeInsets
                                                                      .fromLTRB(
                                                                      19.5,
                                                                      0,
                                                                      0,
                                                                      0),
                                                                  width: 9.3,
                                                                  height: 13,
                                                                  child:
                                                                      const SizedBox(
                                                                    width: 9.3,
                                                                    height: 13,
                                                                    // child: SvgPicture
                                                                    //     .network(
                                                                    //   'assets/vectors/vector_11_x2.svg',
                                                                    // ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: const Color(
                                                                  0xFFFF0000),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                            ),
                                                            child: Container(
                                                              width: 79,
                                                              height: 79,
                                                              child: Positioned(
                                                                right: -31,
                                                                bottom: 0,
                                                                child:
                                                                    Container(
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    image:
                                                                        DecorationImage(
                                                                      fit: BoxFit
                                                                          .cover,
                                                                      image:
                                                                          AssetImage(
                                                                        Assets
                                                                            .icons
                                                                            .splashPNG,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  child:
                                                                      Container(
                                                                    width: 120,
                                                                    height: 79,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              const Positioned(
                                                left: 61.2,
                                                bottom: 0,
                                                child: SizedBox(
                                                  height: 28,
                                                  child: Text(
                                                    'شارع الجبلايه، الزمالك، محافظة القاهرة‬ 4270023',
                                                    textAlign: TextAlign.right,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 12,
                                                      color: Color(0xFF8D8D8D),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
