import 'package:app_test/assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kiwi/kiwi.dart';

import '../../controller/splash_bloc/bloc.dart';

class SplashView extends StatelessWidget {
  const SplashView({super.key});

  @override
  Widget build(BuildContext context) {
    SplashBloc bloc = KiwiContainer().resolve<SplashBloc>()
      ..add(SplashInitialEvent());
    return BlocBuilder(
      bloc: bloc,
      builder: (context, state) {
        return Scaffold(
          body: Center(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(
                    Assets.icons.splashPNG,
                  ),
                ),
              ),
              child: Image.asset(
                Assets.icons.logoPNG,
                width: 100.w,
              ),
            ),
          ),
        );
      },
    );
  }
}
