import '../../assets.dart';
import '../../helpers/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../helpers/custom_btn.dart';
import '../../helpers/route.dart';
import '../../routes/routes.dart';

class AllowLocationView extends StatefulWidget {
  const AllowLocationView({super.key});

  @override
  State<AllowLocationView> createState() => _AllowLocationViewState();
}

class _AllowLocationViewState extends State<AllowLocationView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Spacer(),
            Image.asset(
              Assets.icons.locationPanaPNG,
              width: 200.w,
            ),
            StylesApp.instance.sizedBoxHight,
            Text(
              "نحن بحاجة لتحديد موقعك الجغرافي ",
              style: StylesApp.instance.textStayleOne,
            ),
            StylesApp.instance.sizedBoxHight,
            Text(
              "لغرض تحديد الملعب الاقرب اليك",
              style: StylesApp.instance.textStayleTow,
            ),
            const Spacer(),
            CustomElevatedButton(
              width: double.infinity / 2,
              text: "السماح",
              onPressed: () {
                push(
                  NamedRoutes.i.select_location,
                  type: NavigatorAnimation.scale,
                );
              },
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}
