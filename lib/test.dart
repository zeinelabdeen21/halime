//   cupertino_icons: ^1.0.2
//   flutter_bloc: ^8.1.3
//   bloc: ^8.1.2
//   dio: ^5.4.0
//   flutter_screenutil: ^5.9.0
//   easy_localization: ^3.0.3
//   kiwi: ^4.1.0
//   shimmer: ^3.0.0
//   lottie: 
//   flash: 
//   flutter_svg_provider: 
//   equatable: ^2.0.5
//   liquid_pull_to_refresh: ^3.0.1

// dev_dependencies:
//   flutter_test:
//     sdk: flutter

 
//   flutter_lints: ^2.0.0


// flutter:
//   uses-material-design: true
//   assets:
//     - assets/icons/
//     - assets/fonts/
//     - assets/langs/
//     - assets/langs/en-US.json
//     - assets/langs/ar-SA.json


//   fonts:
//     - family: Somar
//       fonts:
//         - asset: assets/fonts/alfont_com_SomarGX.otf
//     - family: SomarMedium
//       fonts:
//         - asset: assets/fonts/ArbFONTS-Somar-Medium.otf
//     - family: SomarRegular
//       fonts:
//         - asset: assets/fonts/ArbFONTS-Somar-Regular.otf
//     - family: NeoSansArabicRegular
//       fonts:
//         - asset: assets/fonts/NeoSansArabicRegular.ttf
//     - family: NeoSansMedium
//       fonts:
//         - asset: assets/fonts/Neo_Sans_Medium.ttf