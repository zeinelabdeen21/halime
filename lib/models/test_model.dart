// To parse this JSON data, do
//
//     final temperatures = temperaturesFromJson(jsonString);

import 'dart:convert';

Temperatures temperaturesFromJson(String str) =>
    Temperatures.fromJson(json.decode(str));

String temperaturesToJson(Temperatures data) => json.encode(data.toJson());

class Temperatures {
  final List<Datum>? data;
  final String? message;
  final int? status;

  Temperatures({
    this.data,
    this.message,
    this.status,
  });

  factory Temperatures.fromJson(Map<String, dynamic> json) => Temperatures(
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
        message: json["message"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
        "message": message,
        "status": status,
      };
}

class Datum {
  final int? id;
  final String? image;
  final String? titleAr;
  final String? titleEn;
  final String? status;

  Datum({
    this.id,
    this.image,
    this.titleAr,
    this.titleEn,
    this.status,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        image: json["image"],
        titleAr: json["title_ar"],
        titleEn: json["title_en"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
        "title_ar": titleAr,
        "title_en": titleEn,
        "status": status,
      };
}
