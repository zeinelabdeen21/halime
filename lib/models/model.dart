// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  final UserData? data;
  final String? message;
  final int? status;

  LoginModel({
    this.data,
    this.message,
    this.status,
  });

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        data: UserData.fromJson(json["data"]),
        message: json["message"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
        "message": message,
        "status": status,
      };
}

class UserData {
  final int id;
  final String name;
  final String email;
  final String phone;
  final dynamic image;
  final dynamic fcmToken;
  final String status;
  final String isVerified;
  final DateTime createdAt;
  final String token;

  UserData({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.image,
    required this.fcmToken,
    required this.status,
    required this.isVerified,
    required this.createdAt,
    required this.token,
  });

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        phone: json["phone"],
        image: json["image"],
        fcmToken: json["fcm_token"],
        status: json["status"],
        isVerified: json["is_verified"],
        createdAt: DateTime.parse(json["created_at"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phone": phone,
        "image": image,
        "fcm_token": fcmToken,
        "status": status,
        "is_verified": isVerified,
        "created_at": createdAt.toIso8601String(),
        "token": token,
      };
}
