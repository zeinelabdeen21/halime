// this function is repository

import '../data_controller/data_controller.dart';
import '../models/home_model.dart';
import '../services/server_gate.dart';

class RepositoryInterface {
  static final RepositoryInterface instance = RepositoryInterface._internal();

  RepositoryInterface._internal() {
    HomeRepository.instance;
  }

  Future<HomeModel> getData() => HomeRepository.instance.data();
  Future<CustomResponse> fetchData() => HomeRepository.instance.fetchData();
}
