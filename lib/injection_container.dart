import 'package:kiwi/kiwi.dart';

import 'controller/home_bloc/bloc.dart';
import 'controller/sign_In_bloc/bloc.dart';
import 'controller/splash_bloc/bloc.dart';

initKiwi() {
  KiwiContainer container = KiwiContainer();

  container.registerFactory((c) => SplashBloc());
  container.registerFactory((c) => SignInBloc());
  container.registerFactory((c) => HomeBloc());
  container.registerFactory((c) => HomeBlocMvvm());
}
